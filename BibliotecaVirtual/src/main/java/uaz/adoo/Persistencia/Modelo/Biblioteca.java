package uaz.adoo.persistencia.dao.modelo;
public class Biblioteca {
    int id;
    String nombre;
    String editorial;
    String fecha;
    String categoria;

    public void biblioteca(int id, String nombre, String editorial, String fecha, String categoria){
        this.id = id;
        this.nombre = nombre;
        this.editorial = editorial;
        this.fecha = fecha;
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
  
}

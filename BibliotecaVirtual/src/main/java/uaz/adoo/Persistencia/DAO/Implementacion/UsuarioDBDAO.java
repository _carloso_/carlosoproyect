package uaz.adoo.persistencia.dao.implementacion;
import uaz.adoo.persistencia.dao.interfaz.ICatalogoDAO;
import uaz.adoo.persistencia.dao.modelo.Usuario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDBDAO implements ICatalogoDAO<Usuario> {

    private static final Logger logger = Logger.getLogger("UsuarioDBDAO");
    //Informacion de la conexion
    public static final String NOMBRE_TABLA = "Usuario";
    public static final String NOMBRE_BD = "BaseDeDatos.db";
    public static final String CLASE_SQLITE = "org.sqlite.JDBC";
    public static final String URL_SQLITE = "jdbc:sqlite:"+NOMBRE_BD;
    //Informacion de la tabla (nombres campos)
    public static final String FLD_ID = "id";
    public static final String FLD_NOMBRE= "nombre";
    public static final String FLD_CONTRASENA = "contraseña";
    public static final String FLD_CORREO = "correo";
    //Informacion errores
    public static final String ERR_NO_CLASE = "No se pudo cargar la clase ";
    //Informacion de las consultas
    public static final String QRY_SELECT_ALL;
    public static final String QRY_FIND_BY_ID;
    public static final String QRY_INSERT;
    public static final String QRY_UPDATE;
    public static final String QRY_DELETE;
    // En java, para inicializar variables estaticas, se utiliza el bloque "static"
    static {
        QRY_SELECT_ALL = String.format("SELECT * FROM %s ",NOMBRE_TABLA);
        QRY_FIND_BY_ID = String.format("SELECT * FROM %s WHERE %s = ?",NOMBRE_TABLA,FLD_ID);
        QRY_INSERT = String.format("INSERT INTO %s (%s,%s,%s) VALUES (?,?,?)",
                        NOMBRE_TABLA,FLD_NOMBRE,FLD_CONTRASENA,FLD_CORREO);
        QRY_UPDATE = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                        NOMBRE_TABLA,FLD_NOMBRE,FLD_CONTRASENA,FLD_CORREO,FLD_ID);
        QRY_DELETE = String.format("DELETE FROM %s WHERE %s = ?",NOMBRE_TABLA,FLD_ID);
    }
    

    public usuarioDBDAO(){
        try {
            Class.forName(CLASE_SQLITE);
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE,ERR_NO_CLASE + ex.getMessage());
            System.exit(1);
        }
    }

    @Override
    public Usuario find(int idUsuarioDao) {
        Usuario usuario;
        usuario = null;
        logger.info(QRY_FIND_BY_ID);
        try (
                Connection c = DriverManager.getConnection(URL_SQLITE);
                PreparedStatement preparedStatement = c.prepareStatement(QRY_FIND_BY_ID);
        ){
            preparedStatement.setInt(1,idUsuarioDao);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                usuario = crearUsuarioDesdeResultSet(rs);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,ex.getMessage());
        }
        return usuario;
    }

    @Override
    public List<Usuario> findAll() {
        List<Usuario> elUsuario = new ArrayList<>();
        logger.info(QRY_SELECT_ALL);
        try (
            Connection c = DriverManager.getConnection(URL_SQLITE);
            PreparedStatement preparedStatement = c.prepareStatement(QRY_SELECT_ALL);
        ) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Usuario usuario = crearUsuarioDesdeResultSet(rs);
                elUsuario.add(usuario);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,ex.getMessage());
        }
        return elUsuario;
    }

    @Override
    public boolean save(Usuario nuevoUsuario) {
        logger.info(QRY_INSERT);
        try(
            Connection c = DriverManager.getConnection(URL_SQLITE);
            Statement stmt = c.createStatement();
            PreparedStatement preparedStatement = c.prepareStatement(QRY_INSERT);
            )
        {
            preparedStatement.setString(1,nuevoUsuario.getNombre());
            preparedStatement.setString(2,nuevoUsuario.getContrasena());
            preparedStatement.setString(3,nuevoUsuario.getCorreo());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Usuario unUsuario) {
        logger.info(QRY_UPDATE);
        try(
            Connection c = DriverManager.getConnection(URL_SQLITE);
            Statement stmt = c.createStatement();
            PreparedStatement preparedStatement = c.prepareStatement(QRY_UPDATE)
            )
        {
            preparedStatement.setString(1,nuevoUsuario.getNombre());
            preparedStatement.setString(2,nuevoUsuario.getContrasena());
            preparedStatement.setString(3,nuevoUsuario.getCorreo());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Usuario unUsuario) {
        String qryDelete = String.format(QRY_DELETE,unUsuario.getId());
        logger.info(qryDelete);
        try(
                Connection c = DriverManager.getConnection(URL_SQLITE);
                PreparedStatement preparedStatement = c.prepareStatement(QRY_DELETE);
           )
        {
            preparedStatement.setInt(1,unUsuario.getId());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    private Usuario crearUsuarioDesdeResultSet(ResultSet rs){
        Usuario salida;
        try{
            int id = rs.getInt(FLD_ID);
            String nombre = rs.getString(FLD_NOMBRE);
            String contrasena = rs.getString(FLD_CONTRASENA);
            String correo = rs.getString(FLD_CORREO);
            salida = new Productos(id,nombre,contrasena,correo);
            logger.info(salida.toString());
        }catch (SQLException e){
            logger.severe(e.getMessage());
            salida = null;
        }
        return salida;
    }

}
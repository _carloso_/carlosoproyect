package uaz.adoo.persistencia.dao.implementacion;
import uaz.adoo.persistencia.dao.interfaz.ICatalogoDAO;
import uaz.adoo.persistencia.dao.modelo.Biblioteca;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BibliotecaDBDAO implements ICatalogoDAO<Biblioteca> {

    private static final Logger logger = Logger.getLogger("BibliotecaDBDAO");
    //Informacion de la conexion
    public static final String NOMBRE_TABLA = "Biblioteca";
    public static final String NOMBRE_BD = "BaseDeDatos.db";
    public static final String CLASE_SQLITE = "org.sqlite.JDBC";
    public static final String URL_SQLITE = "jdbc:sqlite:"+NOMBRE_BD;
    //Informacion de la tabla (nombres campos)
    public static final String FLD_ID = "id";
    public static final String FLD_NOMBRE= "nombre";
    public static final String FLD_EDITORIAL = "editorial";
    public static final String FLD_FECHA = "fecha";
    public static final String FLD_CATEGORIA = "categoria";
    //Informacion errores
    public static final String ERR_NO_CLASE = "No se pudo cargar la clase ";
    //Informacion de las consultas
    public static final String QRY_SELECT_ALL;
    public static final String QRY_FIND_BY_ID;
    public static final String QRY_INSERT;
    public static final String QRY_UPDATE;
    public static final String QRY_DELETE;
    // En java, para inicializar variables estaticas, se utiliza el bloque "static"
    static {
        QRY_SELECT_ALL = String.format("SELECT * FROM %s ",NOMBRE_TABLA);
        QRY_FIND_BY_ID = String.format("SELECT * FROM %s WHERE %s = ?",NOMBRE_TABLA,FLD_ID);
        QRY_INSERT = String.format("INSERT INTO %s (%s,%s,%s,%s) VALUES (?,?,?,?)",
                        NOMBRE_TABLA,FLD_NOMBRE,FLD_EDITORIAL,FLD_FECHA,FLD_CATEGORIA);
        QRY_UPDATE = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                        NOMBRE_TABLA,FLD_NOMBRE,FLD_EDITORIAL,FLD_FECHA,FLD_CATEGORIA,FLD_ID);
        QRY_DELETE = String.format("DELETE FROM %s WHERE %s = ?",NOMBRE_TABLA,FLD_ID);
    }
    

    public BibliotecaDBDAO(){
        try {
            Class.forName(CLASE_SQLITE);
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE,ERR_NO_CLASE + ex.getMessage());
            System.exit(1);
        }
    }

    @Override
    public Biblioteca find(int idBibliotecaDao) {
        Biblioteca biblioteca;
        biblioteca = null;
        logger.info(QRY_FIND_BY_ID);
        try (
                Connection c = DriverManager.getConnection(URL_SQLITE);
                PreparedStatement preparedStatement = c.prepareStatement(QRY_FIND_BY_ID);
        ){
            preparedStatement.setInt(1,idBibliotecaDao);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                biblioteca = crearLibroDesdeResultSet(rs);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,ex.getMessage());
        }
        return biblioteca;
    }

    @Override
    public List<Biblioteca> findAll() {
        List<Biblioteca> laBiblioteca = new ArrayList<>();
        logger.info(QRY_SELECT_ALL);
        try (
            Connection c = DriverManager.getConnection(URL_SQLITE);
            PreparedStatement preparedStatement = c.prepareStatement(QRY_SELECT_ALL);
        ) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Biblioteca biblioteca = crearLibroDesdeResultSet(rs);
                laBiblioteca.add(biblioteca);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,ex.getMessage());
        }
        return laBiblioteca;
    }

    @Override
    public boolean save(Biblioteca nuevoLibro) {
        logger.info(QRY_INSERT);
        try(
            Connection c = DriverManager.getConnection(URL_SQLITE);
            Statement stmt = c.createStatement();
            PreparedStatement preparedStatement = c.prepareStatement(QRY_INSERT);
            )
        {
            preparedStatement.setString(1,nuevoLibro.getNombre());
            preparedStatement.setString(2,nuevoLibro.getEditorial());
            preparedStatement.setString(3,nuevoLibro.getFecha());
            preparedStatement.setString(4,nuevoLibro.getCategoria());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Biblioteca unLibro) {
        logger.info(QRY_UPDATE);
        try(
            Connection c = DriverManager.getConnection(URL_SQLITE);
            Statement stmt = c.createStatement();
            PreparedStatement preparedStatement = c.prepareStatement(QRY_UPDATE)
            )
        {
            preparedStatement.setString(1,nuevoLibro.getNombre());
            preparedStatement.setString(2,nuevoLibro.getEditorial());
            preparedStatement.setString(3,nuevoLibro.getFecha());
            preparedStatement.setString(4,nuevoLibro.getCategoria());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Biblioteca unLibro) {
        String qryDelete = String.format(QRY_DELETE,unLibro.getId());
        logger.info(qryDelete);
        try(
                Connection c = DriverManager.getConnection(URL_SQLITE);
                PreparedStatement preparedStatement = c.prepareStatement(QRY_DELETE);
           )
        {
            preparedStatement.setInt(1,unLibro.getId());
            preparedStatement.executeUpdate();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    private Biblioteca crearLibroDesdeResultSet(ResultSet rs){
        Biblioteca salida;
        try{
            int id = rs.getInt(FLD_ID);
            String nombre = rs.getString(FLD_NOMBRE);
            String editorial = rs.getString(FLD_EDITORIAL);
            String fecha = rs.getString(FLD_FECHA);
            String categoria = rs.getString(FLD_CATEGORIA);
            salida = new Productos(id,nombre,editorial,fecha,categoria);
            logger.info(salida.toString());
        }catch (SQLException e){
            logger.severe(e.getMessage());
            salida = null;
        }
        return salida;
    }

}


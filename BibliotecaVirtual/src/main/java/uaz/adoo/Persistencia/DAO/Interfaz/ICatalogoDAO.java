package uaz.adoo.persistencia.dao.interfaz;
import java.util.List;
    /*
	find(id ), findById(id)   ---> Regresar un objeto cuyo id le es enviado
	findAll()                  --> Regresa toooooodos los objetos...
	findAllWhere....          --->Encontrar aquellos objetos (registros) que coinciden con un criterio.
	save(objeto)              ---> Guardar un objeto nuevo (un registro nuevo)
	update(objeto)            ---> Actualizar (en el medio de persistencia) el objeto enviado
	delete(objeto), deleteById(id)  ---->Borrar (inactivar)

 */
public interface ICatalogoDAO<E> {
    E find(int id);
    List<E> findAll();
    boolean save(E entidad);
    boolean update(E entidad);
    boolean delete(E entidad);
}


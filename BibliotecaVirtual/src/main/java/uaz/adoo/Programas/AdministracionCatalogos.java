package uaz.adoo.programas;

import uaz.adoo.persistencia.dao.implementacion.UsuarioDBDAO;
import uaz.adoo.persistencia.dao.implementacion.BibliotecaDBDAO;
import uaz.adoo.persistencia.dao.interfaz.ICatalogoDAO;
import uaz.adoo.persistencia.dao.modelo.Usuario;
import uaz.adoo.persistencia.dao.modelo.Biblioteca;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class AdministracionCatalogos {

    private static final Logger log = Logger.getAnonymousLogger();
    private static final Scanner teclado = new Scanner(System.in);
    private static final String NO_TEXTO_ADICIONAL = "";

    private static final String OPCIONES_CATALOGOS[]={
            "1) Agregar Usuario",
            "2) Biblioteca"
    };
    private static final String NOMBRES_CATALOGOS[]={
            "Usuarios",
            "Biblioteca"
    };

    private static final String OPCIONES_MENU[]={
            "1) Listar ",
            "2) Agregar ",
            "3) Borrar  ",
            "4) Buscar ",
            "5) Actualizar  ",
            "6) Salir de "
    };

    public static final String CAT_USUARIOS = "Usuarios";
    public static final int ID_CAT_USUARIOS = 1;
    public static final String CAT_BIBLIOTECA = "Biblioteca";
    public static final int ID_CAT_BIBLIOTECA= 2;

    public static final int LISTAR = 1;
    public static final int AGREGAR = 2;
    public static final int ELIMINAR = 3;
    public static final int BUSCAR = 4;
    public static final int ACTUALIZAR = 5;
    public static final int VALOR_MINIMO = 1;
    public static final int VALOR_MAXIMO = 6;

    private static final Scanner tecladoId = new Scanner(System.in);
    private static final Scanner tecladoNombre = new Scanner(System.in);

    private static final Scanner tecladoEditoril = new Scanner(System.in);
    private static final Scanner tecladoFecha = new Scanner(System.in);
    private static final Scanner tecladoCategoria = new Scanner(System.in);

    private static final Scanner tecladoContrasena = new Scanner(System.in);
    private static final Scanner tecladoCorreo= new Scanner(System.in);

    private static final ICatalogoDAO<Usuario> daoUsuario = new UsuarioDBDAO();
    private static final ICatalogoDAO<Biblioteca> daoBiblioteca = new BibliotecaDBDAO();

    public static void main(String[] args) {
        mostrarMenu(OPCIONES_CATALOGOS,NO_TEXTO_ADICIONAL);
        int catalogoElegido = leerValorEntero(OPCIONES_CATALOGOS.length);
        log.info("Opcion de catalogo "+catalogoElegido);
        String catalogoActual = NOMBRES_CATALOGOS[catalogoElegido - 1];
        int opcionMenuElegida;
        do{
            opcionMenuElegida = leerOpcionMenu(catalogoActual);
            switch(opcionMenuElegida){
                case LISTAR:
                    if(catalogoElegido == ID_CAT_USUARIOS){
                        mostrarListadoUsuarios();
                    }else{
                        if(catalogoElegido == ID_CAT_BIBLIOTECA){
                            mostrarListadoBiblioteca();
                        }
                    }
                    break;
                case AGREGAR:
                    if(catalogoElegido == ID_CAT_USUARIOS){
                        agregarUsuario();
                    }else{
                        if(catalogoElegido == ID_CAT_BIBLIOTECA){
                            agregarLibro();
                        }
                    }
                    break;
                case ELIMINAR:
                    if(catalogoElegido == ID_CAT_USUARIOS){
                        borrarUsuario();
                    }else{
                        if(catalogoElegido == ID_CAT_BIBLIOTECA){
                            borrarLibro();
                        }
                    }
                case BUSCAR:
                    if(catalogoElegido == ID_CAT_USUARIOS){
                        buscarUsuario();
                    }else{
                        if(catalogoElegido == ID_CAT_BIBLIOTECA){
                            buscarLibro();
                        }
                    }
                    break;
                case ACTUALIZAR:
                    if(catalogoElegido == ID_CAT_USUARIOS){
                        actualizarUsuario();
                    }else{
                        if(catalogoElegido == ID_CAT_BIBLIOTECA){
                            actualizarLibro();
                        }
                    }
                    break;
            }
        } while(opcionMenuElegida != VALOR_MAXIMO);
    }

    private static void mostrarMenu(String opciones[], final String textoAdicional){
        for(String i : opciones){
            System.out.println(i + textoAdicional);
        }
    }

    private static int leerValorEntero(){
        int valor;
        valor = teclado.nextInt();
        teclado.nextLine();
        return valor;
    }

    private static int leerValorEntero(int valorMaximo){
        int valor;
        do{
            valor = leerValorEntero();
        }while (valor < AdministracionCatalogos.VALOR_MINIMO || valor > valorMaximo);
        return valor;
    }

    private static int leerOpcionMenu(String nombreCatalogo){
        int respuesta;
        do{
            mostrarMenu(OPCIONES_MENU,nombreCatalogo);
            respuesta = leerValorEntero();
        } while(respuesta < VALOR_MINIMO || respuesta >VALOR_MAXIMO);
        return respuesta;
    }

    //Mostrar listados
    private static void mostrarListadoUsuarios(){
        List<Usuario> elUsuario = daoUsuario.findAll();
        for( Usuario unUsuario : elUsuario){
            System.out.println(unUsuario.toString());
        }
    }

    private static void mostrarListadoBiblioteca(){
        List<Biblioteca> laBiblioteca = daoBiblioteca.findAll();
        for( Biblioteca unLibro : laBiblioteca){
            System.out.println(unLibro.toString());
        }
    }

    // métodos para agregar
    private static void agregarUsuario(){
        int id;
        String nombre, contrasena, correo;
        System.out.print("Escribe el id del usuario: ");
        id = tecladoId.nextInt();
        System.out.print("Escribe el nick del usuario: ");
        nombre = tecladoNombre.nextLine();
        System.out.print("Escribe la contraseña del usuario: ");
        contrasena = tecladoContrasena.nextLine();
        System.out.print("Escribe el correo del usuario: ");
        correo = tecladoCorreo.nextLine();
        Usuario unUsuario = new Usuario(id,nombre,contrasena,correo);
        boolean saveUsuario = daoUsuario.save(unUsuario);
        if (saveUsuario){
            System.out.println("-----Usuario Guardado Correctamente-----");
        } else {
            System.out.println("-----Error al guardar el Usuario-----");
        }
    }

    private static void agregarLibro(){
        int id;
        String nombre, editorial, fecha, categoria;
        System.out.print("Escribe el id del producto: ");
        id = tecladoId.nextInt();
        System.out.print("Escribe el nombre del libro: ");
        nombre = tecladoNombre.nextLine();
        System.out.print("Escribe la editorial del libro: ");
        editorial = tecladoCaducidad.nextLine();
        System.out.print("Escribe la categoria del libro: ");
        categoria = tecladoCategoria.nextLine();
        System.out.print("Escribe la fecha de impresión del libro: ");
        fecha = tecladoCantidad.nextInt();
        Biblioteca unLibro = new Biblioteca(id,nombre,editorial,fecha,categoria);
        boolean saveLibro = daoBiblioteca.save(unLibro);
        if (saveLibro){
            System.out.println("-----Libro Guardado Correctamente-----");
        } else {
            System.out.println("-----Error al guardar el Libro-----");
        }
    }

    // métodos para buscar
    private static void buscarUsuario(){
        int numUsuarioBuscar;
        System.out.print("Escribe el id del Usuario a buscar: ");
        numUsuarioBuscar = leerValorEntero();
        Usuario unUsuario = daoUsuario.find(numUsuarioBuscar);
        if (unUsuario != null) {
            System.out.println("El proveedor buscado es: " + unUsuario);
        } else {
            System.out.println("No se encuentra ese proveedor");
        }
    }

    private static void buscarLibro(){
        int numLibroBuscar;
        System.out.print("Escribe el id del Libro a buscar: ");
        numLibroBuscar = leerValorEntero();
        Biblioteca unLibro = daoBiblioteca.find(numLibroBuscar);
        if (unLibro != null) {
            System.out.println("El producto buscado es: " + unLibro);
        } else {
            System.out.println("No se encuentra ese producto");
        }
    }

    // métodos para borrar
    private static void borrarUsuario(){
        int numUsuarioABorrar;
        System.out.print("Escribe el id del usuario a borrar: ");
        numUsuarioABorrar = leerValorEntero();
        Usuario unUsuario = daoProveedor.find(numUsuarioABorrar);
        if(unUsuario != null){
            System.out.println("Borrando el usuario: " + unUsuario);
            daoProveedor.delete(unUsuario);
        }else{
            System.out.println("No se encuentra ese usuario para borrar");
        }
    }

    private static void borrarLibro(){
        int numLibroABorrar;
        System.out.print("Escribe el id del Libro a borrar: ");
        numLibroABorrar = leerValorEntero();
        Biblioteca unLibro = daoBiblioteca.find(numLibroABorrar);
        if(unLibro != null){
            System.out.println("Borrando el libro: " + unLibro);
            daoProducto.delete(unLibro);
        }else{
            System.out.println("No se encuentra ese libro para borrar");
        }
    }

    // métodos para actualizar
    private static void actualizarUsuario(){
        int numUsuarioActualizar;
        String contrasena, correo;
        System.out.print("Escribe el id del Usuario a actualizar: ");
        numUsuarioActualizar = leerValorEntero();
        Usuario unUsuario = daoUsuario.find(numUsuarioActualizar);
        if (unUsuario != null){
            System.out.print("Escribe la nueva contrasena del usuario: ");
            contrasena = tecladoContrasena.nextLine();
            System.out.print("Escribe el nuevo correo del usuario: ");
            correo = tecladoCorreo.nextLine();
            Usuario usu = new Usuario(unUsuario.getId(),unUsuario.getNombre(),
                    contrasena,correo);
            boolean updateUsuario = daoUsuario.update(usu);
            if (updateUsuario){
                System.out.println("-----Usuario Actualizado Correctamente-----");
            } else {
                System.out.println("-----Error al actualizar el usuario-----");
            }
        } else {
            System.out.println("No se encuentra ese usuario para actualizar");
        }
    }

    private static void actualizarLibro(){
        int numLibroActualizar;
        int id;
        String editorial, fecha;
        System.out.print("Escribe el id del Libro a actualizar: ");
        numLibroActualizar = leerValorEntero();
        Biblioteca unLibro = daoBiblioteca.find(numLibroActualizar);
        if (unLibro != null){
            System.out.print("Escribe la nueva editorial del libro: ");
            editorial = tecladoEditoril.nextLine();
            System.out.print("Escribe la nueva fecha de impresion del libro: ");
            fecha = tecladoFecha.nextLine();
            Biblioteca bib = new Biblioteca(unLibro.getId(),unLibro.getNombre(),editorial,fecha,
            unProducto.getCategoria());
            boolean updateLibro = daoBiblioteca.update(bib);
            if (updateLibro){
                System.out.println("-----Libro Actualizado Correctamente-----");
            } else {
                System.out.println("-----Error al actualizar el libro-----");
            }
        } else {
            System.out.println("No se encuentra ese libro para actualizar");
        }
    }
}

